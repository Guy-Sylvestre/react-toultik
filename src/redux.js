import { configureStore, createSlice } from "@reduxjs/toolkit";

const todoSlide = createSlice({
    name: "todo",
    initialState: [
        { id: 1, text: "Faire les courses", done: false },
        { id: 2, text: "Ménage !", done: true },
    ],
    reducers: {
        addTask: (state, action) => {
            // { Type: "todo/addTask", payload: "Aller faire les courses" }
            const newTask = {
                id: Date.now(),
                done: false,
                text: action.payload
            }
            state.push(newTask);
        },
        toogleTask: (state, action) => {
        // { type: "todo/toogleTask", payload: 20 }
        const task = state.find(t => t.id === action.payload);
        task.done = !task.done;
        },
        deleteTask: (state, action) => {
            // { type: "todo/deleteTask", payload: 20 }
            state = state.filter(t => t.id !== action.payload);
            // return state;
        },

    }
})

const { addTask, deleteTask, toogleTask } = todoSlide.actions;

export const store = configureStore({
    reducer: {
        todo: todoSlide.reducer
    }
})


// Les "actions creators" sont des fonctions qui aident a crer des objects action

// const action = createToggle(20);

// export const createToggle = (id) => {
//     return {
//         type: "todo/toogleTask",
//         payload: id
//     }
// }
