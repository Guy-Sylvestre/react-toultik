import { useSelector } from "react-redux";
import TaskItem from "./TaskItem";

const TasksList = (props) => {
  
  //  Pour recuper les information qui vient de la libririe redux avec useSelector

  const tasks = useSelector((state) => state.todo)

  return (
    <>
      {tasks.map((t) => (
        <TaskItem
          task={t}
          key={t.id}
          toggleTask={props.toggleTask}
          deleteTask={props.deleteTask}
        />
      ))}
    </>
  );
};

export default TasksList;
